<?php 
    $numero = $_GET['numero'];

    if(empty($numero)){
        header('Location: index.php');
    }else{
        $numero = intval($numero);
    }

    if($numero<=0 || $numero>=16) header('Location: index.php?error=novalido');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de multiplicar</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

    <div class="encabezado">
        <h2>Usted ha ingresado el número <strong><?php echo $numero?></strong></h2>
        <hr>
        <div class="pull-left">
            <p>La tabla de multiplicar es: </p>
        </div>
    </div>
    <div class="cuerpo">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead >
                    <tr>
                        <th scope="col">Número</th>
                        <th scope="col">X</th>
                        <th scope="col">i</th>
                        <th scope="col">Resultado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i=1; $i<=10; $i++){ ?>
                        <tr>
                            <th><?php echo $numero?></th>
                            <td> X </td>
                            <td><?php echo $i ?></td>
                            <td><?php echo $numero*$i ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <a href="index.php" class="btn btn-block" style="background-color: #F59943; color:white" >Volver al inicio</a>
    </div>
    


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>