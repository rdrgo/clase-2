<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clase 2</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="section">
        <div class="encabezado">
            <h2>Ingrese un número y vea su tabla de multiplicar</h2>
            <hr>
            <div class="pull-left">
                <p>Por favor ingrese números entre el 1 y el 15.</p>
            </div>
        </div>
        <div class="cuerpo">
            <form action="tabla.php" method="GET">
                <div class="row">
                    <div class="form-group col-md-6">
                        <input type="number" class="form-control" placeholder="Ingrese un número" id="numero" name="numero" maxlength="2" autofocus>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-block" style="background-color: #F59943; color:white">Ingresar</button>
                    </div>
                </div>    
            </form>
            <?php 
                if(!empty($_GET['error'])){
                    if($_GET['error']=='novalido'){  
            ?>
                    <div class="alert alert-danger col-md-12" role="alert">
                        Ingrese un número válido! recuerde que debe ser entre 1 y 15.
                    </div>
            <?php 
                    }
                }
            ?>



        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>